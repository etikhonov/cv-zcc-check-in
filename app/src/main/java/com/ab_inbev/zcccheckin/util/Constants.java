package com.ab_inbev.zcccheckin.util;

public class Constants {
    public static final String PREF_KEY_BACKGROUND = "pref_welcome_screen_background_path";
    public static final String PREF_KEY_LOGO = "pref_welcome_logo_path";
    public static final String PREF_KEY_WELCOME_MSG = "pref_welcome_text";
    public static final String PREF_KEY_USE_WELCOME = "pref_key_use_welcome_screen";
    public static final String PREF_KEY_SYNC_FREQUENCY = "pref_sync_frequency";
    public static final String PREF_KEY_GOOGLE_SHEET_ID = "pref_google_sheet_id";
    public static final String PREF_KEY_DATA_CLEANSING = "pref_data_cleansing_period";
    public static final String PREF_KEY_CHECKPOINT = "pref_checkpoint_name";

    public static final String EXTRA_VISITOR = "EXTRA_VISITOR";

    public static final String GOOGLE_SCRIPT_ID = "";
}

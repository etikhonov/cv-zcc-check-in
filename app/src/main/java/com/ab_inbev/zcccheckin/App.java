package com.ab_inbev.zcccheckin;

import android.app.Application;

import com.ab_inbev.zcccheckin.repository.db.AppDatabase;
import com.google.firebase.analytics.FirebaseAnalytics;

public class App extends Application {

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate() {
        super.onCreate();
        AppDatabase.getInstance(this);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

}

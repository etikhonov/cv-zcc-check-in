package com.ab_inbev.zcccheckin.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.ab_inbev.zcccheckin.R;
import com.ab_inbev.zcccheckin.model.Visitor;
import com.ab_inbev.zcccheckin.repository.VisitorsRepository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ETikhonov on 2018-01-14
 * Dialog for check-out
 */

public class CheckOutDialogFragment extends DialogFragment {

    ViisitorDialogListener mListener;
    Visitor selectedVisitor;
    View view;
    private List<Visitor> mVisitors = new ArrayList<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.dialog_checkout, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view)
                .setPositiveButton(R.string.checkout, null)
                .setNegativeButton(R.string.cancel, null)
                .setOnKeyListener((DialogInterface dialogInterface, int i, KeyEvent keyEvent) -> {
                    if (i == KeyEvent.KEYCODE_BACK) {
                        mListener.onDialogNegativeClick(CheckOutDialogFragment.this);
                        return false;
                    }
                    return false;
                });

        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnShowListener((DialogInterface dialogInterface) -> {
            Button buttonPositive = ((AlertDialog) dialogInterface).getButton(DialogInterface.BUTTON_POSITIVE);
            buttonPositive.setOnClickListener((View v) -> {
                if (selectedVisitor == null) {
                    setError();
                } else {
                    mListener.onDialogPositiveClick(CheckOutDialogFragment.this, selectedVisitor);
                    this.dismiss();
                }
            });
            Button buttonNegative = ((AlertDialog) dialogInterface).getButton(DialogInterface.BUTTON_NEGATIVE);
            buttonNegative.setOnClickListener((View v) -> {
                mListener.onDialogNegativeClick(CheckOutDialogFragment.this);
                this.dismiss();
            });
        });
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        wmlp.y = 50;
        dialog.getWindow().setAttributes(wmlp);

        VisitorsRepository visitorsRepository = new VisitorsRepository(getContext());

        AutoCompleteTextView autoCompleteTextView = view.findViewById(R.id.dout_visitorName);
        autoCompleteTextView.setOnItemClickListener(
                (AdapterView<?> parent, View v, int position, long id) -> {
                    selectedVisitor = (Visitor) parent.getItemAtPosition(position);
                    removeError();
                });

        compositeDisposable.add(
                visitorsRepository.getVisitorsInOffice()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe((List<Visitor> list) -> {
                            mVisitors.addAll(list);
                            ArrayAdapter adapter = new ArrayAdapter<>(getContext(),
                                    android.R.layout.simple_spinner_dropdown_item, mVisitors);
                            autoCompleteTextView.setAdapter(adapter);
                        })
        );

        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (ViisitorDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement CheckOutDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.onDialogNegativeClick(CheckOutDialogFragment.this);
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void setError() {
        TextInputLayout inputLayoutVisitorsName = view.findViewById(R.id.inputLayoutVisitorsName);
        inputLayoutVisitorsName.setError(getString(R.string.error_select_name));
        inputLayoutVisitorsName.setErrorEnabled(true);
    }

    public void removeError() {
        TextInputLayout inputLayoutVisitorsName = view.findViewById(R.id.inputLayoutVisitorsName);
        inputLayoutVisitorsName.setError(null);
        inputLayoutVisitorsName.setErrorEnabled(false);
    }

}

package com.ab_inbev.zcccheckin.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import com.ab_inbev.zcccheckin.R;

/**
 * Created by ETikhonov on 2018-02-03.
 * DialogFragment with About App Info
 */
public class AboutDialogFragment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getContext();
        View view = getActivity()
                .getLayoutInflater()
                .inflate(R.layout.dialog_about, null);

        String versionName = getString(R.string.version);
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager != null) {
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo(getContext().getPackageName(), 0);
                    versionName += packageInfo.versionName + " (" + packageInfo.versionCode + ")";
                } catch (PackageManager.NameNotFoundException e) {
                    versionName += "NA";
                }
            }
        } catch (NullPointerException npe) {
            versionName += "NA";
        }
        ((TextView) view.findViewById(R.id.aboutVersion)).setText(versionName);
        view.findViewById(R.id.about_contact_btn).setOnClickListener(v -> sendMail(v));
        view.findViewById(R.id.about_issue_btn).setOnClickListener(v -> sendMail(v));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        return builder.create();

    }

    private void sendMail(View view) {
        String mail = "";
        if (view.getId() == R.id.about_contact_btn) {
            mail = "mailto:evgeniy.tikhonov@gmail.com";
        } else if (view.getId() == R.id.about_issue_btn) {
            mail = "mailto:incoming+etikhonov/ZCCCheckin@gitlab.com";
        }

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse(mail));
        intent.putExtra(Intent.EXTRA_SUBJECT, "[ZCCCheckin] ");
        try {
            startActivity(Intent.createChooser(intent, ""));
        } catch (android.content.ActivityNotFoundException ex) {
            Snackbar.make(view, ex.getMessage(), Snackbar.LENGTH_SHORT);
        }
    }
}

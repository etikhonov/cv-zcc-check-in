package com.ab_inbev.zcccheckin.view;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import com.ab_inbev.zcccheckin.util.Constants;

public class LauncherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean useWelcomeScreen = PreferenceManager
                .getDefaultSharedPreferences(this)
                .getBoolean(Constants.PREF_KEY_USE_WELCOME, true);

        if (useWelcomeScreen) {
            Intent intent = new Intent(this, WelcomeScreenActivity.class);
            finish();
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, VisitorsListActivity.class);
            finish();
            startActivity(intent);
        }
    }
}

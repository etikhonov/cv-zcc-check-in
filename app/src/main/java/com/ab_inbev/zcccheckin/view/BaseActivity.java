package com.ab_inbev.zcccheckin.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.ab_inbev.zcccheckin.util.Constants;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void loadCustomBackGround(View v) {
        String backGroundPath = PreferenceManager
                .getDefaultSharedPreferences(this)
                .getString(Constants.PREF_KEY_BACKGROUND, "def");
        setImage(v, backGroundPath);
    }

    public void loadCustomLogo(View v) {
        String logoPath = PreferenceManager
                .getDefaultSharedPreferences(this)
                .getString(Constants.PREF_KEY_LOGO, "def");
        setImage(v, logoPath);
    }

    private void setImage(View v, String imgPath) {
        Uri uri = Uri.parse(imgPath);
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        final int takeFlags = intent.getFlags()
                & Intent.FLAG_GRANT_READ_URI_PERMISSION;
        try {
            getContentResolver().takePersistableUriPermission(uri, takeFlags);
            ParcelFileDescriptor parcelFileDescriptor =
                    getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            Drawable d = new BitmapDrawable(getResources(), image);
            if (v instanceof ImageView) {
                ((ImageView) v).setImageDrawable(d);
            } else {
                v.setBackground(d);
            }
            parcelFileDescriptor.close();
        } catch (SecurityException se) {
            //TODO customImage SecurityException
        } catch (FileNotFoundException fnfe) {
            //TODO customImage FileNotFoundException
        } catch (IOException ioe) {
            //TODO customImage IOException
        } finally {
            //TODO customImage finnaly
        }

    }

}
package com.ab_inbev.zcccheckin.view;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.ab_inbev.zcccheckin.R;
import com.ab_inbev.zcccheckin.util.Constants;

import java.util.List;

public class SettingsActivity extends AppCompatPreferenceActivity {

    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener =
            (Preference preference, Object value) -> {
                String stringValue = value.toString();

                if (preference instanceof ListPreference) {

                    ListPreference listPreference = (ListPreference) preference;
                    int index = listPreference.findIndexOfValue(stringValue);

                    preference.setSummary(
                            index >= 0
                                    ? listPreference.getEntries()[index]
                                    : null);

                } else {
                    preference.setSummary(stringValue);
                }
                return true;
            };

    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    private static void bindPreferenceSummaryToValue(Preference preference) {

        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set layout top marging from Actionbar
        ViewGroup root = findViewById(android.R.id.content);
        View content = root.getChildAt(0);
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) content.getLayoutParams();
        params.setMargins(0, 192, 0, 0); //xxxhdpi default ActionBar height
        content.setLayoutParams(params);
    }

    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || GeneralPreferenceFragment.class.getName().equals(fragmentName)
                || DataSyncPreferenceFragment.class.getName().equals(fragmentName);
    }

    public static class DataSyncPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_data_sync);
            setHasOptionsMenu(true);

            bindPreferenceSummaryToValue(findPreference(Constants.PREF_KEY_SYNC_FREQUENCY));
            bindPreferenceSummaryToValue(findPreference(Constants.PREF_KEY_GOOGLE_SHEET_ID));

        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }

    public static class GeneralPreferenceFragment extends PreferenceFragment
            implements Preference.OnPreferenceClickListener {

        private static final int PICK_BACKGROUND_REQUEST_CODE = 777;
        private static final int PICK_LOGO_REQUEST_CODE = 999;

        private Preference mWelcomeScreenBackgroundPreference;
        private Preference mLogoPreference;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            setHasOptionsMenu(true);

            mWelcomeScreenBackgroundPreference = findPreference(Constants.PREF_KEY_BACKGROUND);
            mWelcomeScreenBackgroundPreference.setOnPreferenceClickListener(this);

            mLogoPreference = findPreference(Constants.PREF_KEY_LOGO);
            mLogoPreference.setOnPreferenceClickListener(this);

            //TODO loop?
            bindPreferenceSummaryToValue(findPreference(Constants.PREF_KEY_CHECKPOINT));
            bindPreferenceSummaryToValue(findPreference(Constants.PREF_KEY_DATA_CLEANSING));
            bindPreferenceSummaryToValue(findPreference(Constants.PREF_KEY_BACKGROUND));
            bindPreferenceSummaryToValue(findPreference(Constants.PREF_KEY_LOGO));
            bindPreferenceSummaryToValue(findPreference(Constants.PREF_KEY_WELCOME_MSG));

        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        @Override
        public boolean onPreferenceClick(Preference preference) {
            permissionsCheck();
            if (preference.getKey().equals(Constants.PREF_KEY_BACKGROUND)) {
                pickImage(PICK_BACKGROUND_REQUEST_CODE);
            }
            if (preference.getKey().equals(Constants.PREF_KEY_LOGO)) {
                pickImage(PICK_LOGO_REQUEST_CODE);
            }
            return true;
        }

        private void pickImage(int requestCode) {
            Intent intent;
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, ""), requestCode);
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == RESULT_OK && data != null) {
                Uri uri = data.getData();
                final int takeFlags = data.getFlags() & (Intent.FLAG_GRANT_READ_URI_PERMISSION);
                getContext().getContentResolver().takePersistableUriPermission(uri, takeFlags);
                switch (requestCode) {
                    case PICK_BACKGROUND_REQUEST_CODE:
                        savePreference(Constants.PREF_KEY_BACKGROUND, uri);
                        break;
                    case PICK_LOGO_REQUEST_CODE:
                        savePreference(Constants.PREF_KEY_LOGO, uri);
                        break;
                }
            }
        }

        private void savePreference(String prefKey, Uri uri) {
            SharedPreferences preferences
                    = PreferenceManager.getDefaultSharedPreferences(getContext());
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(prefKey, String.valueOf(uri));
            editor.apply();
            sBindPreferenceSummaryToValueListener.onPreferenceChange(findPreference(prefKey), String.valueOf(uri));
        }

        public void permissionsCheck() {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
    }
}

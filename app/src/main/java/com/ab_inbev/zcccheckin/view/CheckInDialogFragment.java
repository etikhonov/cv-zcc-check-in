package com.ab_inbev.zcccheckin.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.ab_inbev.zcccheckin.R;
import com.ab_inbev.zcccheckin.model.Visitor;
import com.ab_inbev.zcccheckin.util.Constants;

/**
 * Created by ETikhonov on 2018-01-13.
 * DialogFragment for Inputting information about visitor
 */

public class CheckInDialogFragment extends DialogFragment {

    View mView;
    EditText mTextIdCard;
    EditText mTextVisitorName;
    EditText mTextVisitorManager;
    EditText mTextVisitorComment;
    Spinner mSpinnerDepartments;
    TextInputLayout mInputLayoutName;

    ViisitorDialogListener mListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        mView = inflater.inflate(R.layout.dialog_checkin, null);
        mTextIdCard = mView.findViewById(R.id.din_idCard);
        mTextVisitorName = mView.findViewById(R.id.din_visitorName);
        mTextVisitorManager = mView.findViewById(R.id.din_visitorManager);
        mTextVisitorComment = mView.findViewById(R.id.din_visitorComment);
        mSpinnerDepartments = mView.findViewById(R.id.din_spinnerDepartments);
        mInputLayoutName = mView.findViewById(R.id.din_inputLayoutVisitorsName);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(mView)
                .setPositiveButton(R.string.checkin, null)
                .setNegativeButton(R.string.cancel, null)
                .setOnKeyListener((DialogInterface dialogInterface, int i, KeyEvent keyEvent) -> {
                    if (i == KeyEvent.KEYCODE_BACK) {
                        mListener.onDialogNegativeClick(CheckInDialogFragment.this);
                        return false;
                    }
                    return false;
                });

        //TODO load data from API
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.departments_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mSpinnerDepartments.setAdapter(adapter);

        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnShowListener((DialogInterface dialogInterface) -> {

            Button buttonPositive = ((AlertDialog) dialogInterface).getButton(DialogInterface.BUTTON_POSITIVE);
            buttonPositive.setOnClickListener((View v) -> {
                if (!mTextVisitorName.getText().toString().isEmpty()) {
                    String checkPointName = PreferenceManager
                            .getDefaultSharedPreferences(getContext())
                            .getString(Constants.PREF_KEY_CHECKPOINT, "");
                    Visitor visitor = new Visitor();
                    int cardId;
                    try {
                        cardId = Integer.parseInt(mTextIdCard.getText().toString());
                    } catch (NumberFormatException nfe) {
                        cardId = 0;
                    }
                    visitor.setIdCardNumber(cardId);
                    visitor.setVisitorsName(mTextVisitorName.getText().toString());
                    visitor.setManager(mTextVisitorManager.getText().toString());
                    visitor.setComments(mTextVisitorComment.getText().toString());
                    visitor.setDepartment(mSpinnerDepartments.getSelectedItem().toString());
                    visitor.setCheckpoint(checkPointName);
                    mListener.onDialogPositiveClick(CheckInDialogFragment.this, visitor);
                    this.dismiss();
                }
            });

            Button buttonNegative = ((AlertDialog) dialogInterface).getButton(DialogInterface.BUTTON_NEGATIVE);
            buttonNegative.setOnClickListener((View v) -> {
                mListener.onDialogNegativeClick(CheckInDialogFragment.this);
                this.dismiss();
            });
        });
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        wmlp.y = 50;
        dialog.getWindow().setAttributes(wmlp);

        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (ViisitorDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement CheckInDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener.onDialogNegativeClick(CheckInDialogFragment.this);
    }

}


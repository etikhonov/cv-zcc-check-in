package com.ab_inbev.zcccheckin.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ab_inbev.zcccheckin.R;
import com.ab_inbev.zcccheckin.adapters.VisitorsRecyclerViewAdapter;
import com.ab_inbev.zcccheckin.model.Visitor;
import com.ab_inbev.zcccheckin.repository.VisitorsRepository;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class VisitorsListActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, ViisitorDialogListener {

    private static final String TAG_CHECKIN = "CheckInDialogFragment";
    private static final String TAG_ABOUT = "AboutDialogFragment";

    @BindView(R.id.vl_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.ivl_checkInDate)
    TextView mCheckInDate;
    @BindView(R.id.ivl_checkInTime)
    TextView mCheckInTime;
    @BindView(R.id.ivl_cardId)
    TextView mCardIdTextView;
    @BindView(R.id.ivl_department)
    TextView mDepartmentTextView;
    @BindView(R.id.ivl_visitorName)
    TextView mVisitorNameTextView;
    @BindView(R.id.ivl_manager)
    TextView mManagerNameTextView;
    @BindView(R.id.ivl_checkOutDate)
    TextView mCheckOutDate;
    @BindView(R.id.ivl_comment)
    TextView mComment;
    @BindView(R.id.vl_drawer)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.vl_nav_view)
    NavigationView mNavigationView;
    @BindView(R.id.vl_layout)
    LinearLayout mContentView;

    VisitorsRepository visitorsRepository = new VisitorsRepository(this);
    private VisitorsRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Visitor> mVisitors;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitors_list);

        //UI
        ButterKnife.bind(this);
        loadCustomBackGround(mContentView);
        mNavigationView.setNavigationItemSelectedListener(this);
        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
                loadCustomDrawer();
            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                SwitchCompat s = findViewById(R.id.vl_nav_show_only_present);
                s.setOnCheckedChangeListener((CompoundButton buttonView, boolean isChecked) ->
                        showOnlyPresentVisitors(buttonView, isChecked));
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        setRecyclerHeader();

        //get Data to grid
        mVisitors = new ArrayList<>();
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(this, 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new VisitorsRecyclerViewAdapter();
        mRecyclerView.setAdapter(mAdapter);
        compositeDisposable.add(
                visitorsRepository.getVisitors()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(list -> {
                            mVisitors.clear();
                            mVisitors.addAll(list);
                            mAdapter.updateVisitorsList(mVisitors);
                        })
        );
    }

    private void setRecyclerHeader() {
        mCheckInDate.setText(R.string.checkInDateCaption);
        mCheckInTime.setText(R.string.checkInTimeCaption);
        mCardIdTextView.setText(R.string.cardIdCaption);
        mVisitorNameTextView.setText(R.string.visitorNameCaption);
        mDepartmentTextView.setText(R.string.departmentCaption);
        mManagerNameTextView.setText(R.string.managerNameCaption);
        mCheckOutDate.setText(R.string.checkOutDateTimeCaption);
        mComment.setText(R.string.comentsCaption);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.vl_nav_item_sync_now:
                visitorsRepository.SyncWithGoogleSheet()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe();
                break;
            case R.id.vl_nav_item_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.vl_nav_item_help:
                DialogFragment dialog = new AboutDialogFragment();
                dialog.show(getSupportFragmentManager(), TAG_ABOUT);
                break;
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showOnlyPresentVisitors(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            compositeDisposable.add(
                    visitorsRepository.getVisitorsInOffice()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(list -> mAdapter.updateVisitorsList(list))
            );
        } else {
            mAdapter.updateVisitorsList(mVisitors);
        }
    }

    @OnClick(R.id.vl_fabAddVisitor)
    public void AddVisitor(View view) {
        DialogFragment dialog = new CheckInDialogFragment();
        dialog.show(getSupportFragmentManager(), TAG_CHECKIN);
    }

    private void loadCustomDrawer() {
        loadCustomBackGround(findViewById(R.id.drawer_header_layout));
        loadCustomLogo(findViewById(R.id.drawer_header_logo));
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, Visitor visitor) {
        if (visitor != null) {
            VisitorsRepository visitorsRepository =
                    new VisitorsRepository(this);
            visitorsRepository.storeVisitorInDb(visitor).subscribe();
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }

}

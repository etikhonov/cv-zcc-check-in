package com.ab_inbev.zcccheckin.view;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ab_inbev.zcccheckin.R;
import com.ab_inbev.zcccheckin.model.Visitor;
import com.ab_inbev.zcccheckin.repository.VisitorsRepository;
import com.ab_inbev.zcccheckin.util.Constants;
import com.example.circulardialog.CDialog;
import com.example.circulardialog.extras.CDConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;

import static com.ab_inbev.zcccheckin.util.Constants.EXTRA_VISITOR;

/**
 * A full-screen activity that allow guests to check-in or check-out
 */
public class WelcomeScreenActivity extends BaseActivity implements ViisitorDialogListener {

    private static final String TAG_CHECKOUT = "CheckOutDialogFragment";
    private static final String TAG_CHECKIN = "CheckInDialogFragment";
    @BindView(R.id.checkInButton)
    ImageButton checkInButton;
    @BindView(R.id.fullscreen_content)
    View mContentView;
    @BindView(R.id.wc_logoImg)
    ImageView mLogoImage;
    @BindView(R.id.wc_welcome_text)
    TextView mWelcomeText;
    VisitorsRepository visitorsRepository;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        loadCustomBackGround(mContentView);
        loadCustomLogo(mLogoImage);

        String prefWelcomeMsg = PreferenceManager
                .getDefaultSharedPreferences(this)
                .getString(Constants.PREF_KEY_WELCOME_MSG, "def");
        if (!prefWelcomeMsg.equals("def")) {
            mWelcomeText.setText(prefWelcomeMsg);
        }
        visitorsRepository =
                new VisitorsRepository(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        showInFullScrean();
    }

    @OnClick(R.id.checkInButton)
    public void showCheckInDialog(View view) {
        DialogFragment dialog = new CheckInDialogFragment();
        dialog.show(getSupportFragmentManager(), TAG_CHECKIN);
        showInFullScrean();
    }

    @OnClick(R.id.checkOutButton)
    public void showCheckOutDialog(View view) {
        DialogFragment dialog = new CheckOutDialogFragment();
        dialog.show(getSupportFragmentManager(), TAG_CHECKOUT);
        showInFullScrean();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, Visitor visitor) {
        showInFullScrean();
        if (dialog instanceof CheckOutDialogFragment) {
            if (visitor != null) {
                visitor.setCheckoutDateTime();
                compositeDisposable.add(
                        //TODO Handle errors
                        visitorsRepository.updateVisitor(visitor)
                                .subscribe(it ->
                                        showSuccessMsg()
                                )
                );
            }
        }
        if (dialog instanceof CheckInDialogFragment) {
            VisitorsRepository visitorsRepository = new VisitorsRepository(this);
            compositeDisposable.add(
                    //TODO Handle errors
                    visitorsRepository.storeVisitorInDb(visitor)
                            .subscribe(it -> {
                                showSuccessMsg();
                                showInFullScrean();
                            })
            );
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        showInFullScrean();
    }

    private void showInFullScrean() {
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    @OnClick(R.id.fabVisitorsList)
    public void showVisitorsList(View view) {
        Intent intent = new Intent(this, VisitorsListActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            Visitor visitor = data.getParcelableExtra(EXTRA_VISITOR);
            VisitorsRepository visitorsRepository = new VisitorsRepository(this);
            compositeDisposable.add(
                    //TODO Handle errors
                    visitorsRepository.storeVisitorInDb(visitor)
                            .subscribe(it -> new CDialog(this)
                                    .createAlert(getResources().getString(R.string.success),
                                            CDConstants.SUCCESS,   // Type of dialog
                                            CDConstants.LARGE)    //  size of dialog
                                    .setAnimation(CDConstants.SCALE_FROM_BOTTOM_TO_TOP)     //  Animation for enter/exit
                                    .setDuration(2500)   // in milliseconds
                                    .setTextSize(CDConstants.LARGE_TEXT_SIZE)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                                    .show())
            );
        }
    }

    @OnClick(R.id.fabSettings)
    public void startSettingsActivity(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    private void showSuccessMsg() {
        new CDialog(this)
                .createAlert(getResources().getString(R.string.success),
                        CDConstants.SUCCESS,   // Type of dialog
                        CDConstants.LARGE)    //  size of dialog
                .setAnimation(CDConstants.SCALE_FROM_BOTTOM_TO_TOP)     //  Animation for enter/exit
                .setDuration(1500)   // in milliseconds
                .setTextSize(CDConstants.LARGE_TEXT_SIZE) // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                .show();
    }
}

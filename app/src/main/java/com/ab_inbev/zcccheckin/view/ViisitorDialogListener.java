package com.ab_inbev.zcccheckin.view;

import android.support.v4.app.DialogFragment;

import com.ab_inbev.zcccheckin.model.Visitor;

public interface ViisitorDialogListener {
    void onDialogPositiveClick(DialogFragment dialog, Visitor visitor);

    void onDialogNegativeClick(DialogFragment dialog);
}

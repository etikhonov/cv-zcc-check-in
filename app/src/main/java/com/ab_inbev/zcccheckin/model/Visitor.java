package com.ab_inbev.zcccheckin.model;

import android.arch.persistence.room.Entity;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import java.util.UUID;

/**
 * Created by ETikhonov on 2018-01-15
 * 2018-02-04 add field checkpoint
 * 2018-02-15 add field uuid
 */

@Entity(tableName = "tblVisitors",
        primaryKeys = {"uuid"})
public class Visitor implements Parcelable {

    public static final Creator<Visitor> CREATOR = new Creator<Visitor>() {
        @Override
        public Visitor createFromParcel(Parcel source) {
            return new Visitor(source);
        }

        @Override
        public Visitor[] newArray(int size) {
            return new Visitor[size];
        }
    };

    @NonNull
    private String uuid = UUID.randomUUID().toString();
    @NonNull
    private LocalDate checkInDate = new LocalDate();
    @NonNull
    private LocalTime checkInTime = new LocalTime();
    private int idCardNumber;
    @NonNull
    private String visitorsName = "";
    private String department;
    private String manager;
    private LocalDateTime checkoutDateTime;
    private String comments;
    private boolean isSynced = false;
    private String checkpoint;

    public Visitor() {
    }

    public Visitor(int idCardNumber,
                   @NonNull String visitorsName,
                   String department,
                   String manager,
                   String comments,
                   String checkpoint) {
        this.uuid = UUID.randomUUID().toString();
        this.idCardNumber = idCardNumber;
        this.visitorsName = visitorsName;
        this.department = department;
        this.manager = manager;
        this.comments = comments;
        this.checkpoint = checkpoint;
    }

    protected Visitor(Parcel in) {
        this.uuid = in.readString();
        this.checkInDate = (LocalDate) in.readSerializable();
        this.checkInTime = (LocalTime) in.readSerializable();
        this.idCardNumber = in.readInt();
        this.visitorsName = in.readString();
        this.department = in.readString();
        this.manager = in.readString();
        this.checkoutDateTime = (LocalDateTime) in.readSerializable();
        this.comments = in.readString();
        this.isSynced = in.readByte() != 0;
        this.checkpoint = in.readString();
    }

    @NonNull
    public String getUuid() {
        return uuid;
    }

    public void setUuid(@NonNull String uuid) {
        this.uuid = uuid;
    }

    @NonNull
    public LocalDate getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(LocalDate checkInDate) {
        this.checkInDate = checkInDate;
    }

    @NonNull
    public LocalTime getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(LocalTime checkInTime) {
        this.checkInTime = checkInTime;
    }

    public int getIdCardNumber() {
        return idCardNumber;
    }

    public void setIdCardNumber(int idCardNumber) {
        this.idCardNumber = idCardNumber;
    }

    @NonNull
    public String getVisitorsName() {
        return visitorsName;
    }

    public void setVisitorsName(@NonNull String visitorsName) {
        this.visitorsName = visitorsName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public LocalDateTime getCheckoutDateTime() {
        return checkoutDateTime;
    }

    public void setCheckoutDateTime(LocalDateTime checkoutDateTime) {
        this.checkoutDateTime = checkoutDateTime;
    }

    public void setCheckoutDateTime() {
        this.checkoutDateTime = LocalDateTime.now(DateTimeZone.getDefault());
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public boolean isSynced() {
        return isSynced;
    }

    public void setSynced(boolean synced) {
        isSynced = synced;
    }

    public String getCheckpoint() {
        return checkpoint;
    }

    public void setCheckpoint(String checkpoint) {
        this.checkpoint = checkpoint;
    }

    @Override
    public String toString() {
        return visitorsName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uuid);
        dest.writeSerializable(this.checkInDate);
        dest.writeSerializable(this.checkInTime);
        dest.writeInt(this.idCardNumber);
        dest.writeString(this.visitorsName);
        dest.writeString(this.department);
        dest.writeString(this.manager);
        dest.writeSerializable(this.checkoutDateTime);
        dest.writeString(this.comments);
        dest.writeByte(this.isSynced ? (byte) 1 : (byte) 0);
        dest.writeString(this.checkpoint);
    }
}

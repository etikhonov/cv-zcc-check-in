package com.ab_inbev.zcccheckin.viewmodel;

import com.ab_inbev.zcccheckin.model.Visitor;
import com.ab_inbev.zcccheckin.repository.VisitorsRepository;

import java.util.List;

import io.reactivex.Observable;

public class VisitorsListViewModel {

    private VisitorsRepository visitorsRepository;

    public VisitorsListViewModel(VisitorsRepository visitorsRepository) {
        this.visitorsRepository = visitorsRepository;
    }

    public Observable<List<Visitor>> getVisitors() {
        return visitorsRepository.getVisitors();
    }
}


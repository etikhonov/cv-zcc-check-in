package com.ab_inbev.zcccheckin.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ab_inbev.zcccheckin.R;
import com.ab_inbev.zcccheckin.model.Visitor;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ETikhonov on 2018-01-15.
 * RecyclerViewAdapter for Visitors List
 */

public class VisitorsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Visitor> mVisitors = new ArrayList<>();

    public VisitorsRecyclerViewAdapter() {
        //Empty Constructor
    }

    public void updateVisitorsList(List<Visitor> visitors) {
        mVisitors.clear();
        mVisitors.addAll(visitors);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_visitor, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Visitor visitor = mVisitors.get(position);
        ItemViewHolder itemHolder = (ItemViewHolder) holder;

        itemHolder.mCheckInDate.setText(visitor.getCheckInDate().toString());
        itemHolder.mCheckInTime.setText(visitor.getCheckInTime().toString("HH:mm"));
        itemHolder.mCardIdTextView.setText(String.valueOf(visitor.getIdCardNumber()));
        itemHolder.mVisitorNameTextView.setText(visitor.getVisitorsName());
        itemHolder.mDepartmentTextView.setText(visitor.getDepartment());
        itemHolder.mManagerNameTextView.setText(visitor.getManager());
        if (mVisitors.get(position).getCheckoutDateTime() != null) {
            itemHolder.mCheckOutDate.setText(visitor.getCheckoutDateTime().toString("yyyy-MM-dd HH:mm"));
        } else {
            itemHolder.mCheckOutDate.setText("");
        }
        itemHolder.mComment.setText(visitor.getComments());
        if (visitor.isSynced()) {
            itemHolder.isSyncedIndicator.setBackgroundResource(R.color.isSyncedTrue);
        } else {
            itemHolder.isSyncedIndicator.setBackgroundResource(R.color.isSyncedFalse);
        }
    }

    @Override
    public int getItemCount() {
        return mVisitors.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivl_checkInDate)
        TextView mCheckInDate;
        @BindView(R.id.ivl_checkInTime)
        TextView mCheckInTime;
        @BindView(R.id.ivl_cardId)
        TextView mCardIdTextView;
        @BindView(R.id.ivl_department)
        TextView mDepartmentTextView;
        @BindView(R.id.ivl_visitorName)
        TextView mVisitorNameTextView;
        @BindView(R.id.ivl_manager)
        TextView mManagerNameTextView;
        @BindView(R.id.ivl_checkOutDate)
        TextView mCheckOutDate;
        @BindView(R.id.ivl_comment)
        TextView mComment;
        @BindView(R.id.ivl_isSynced)
        View isSyncedIndicator;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}

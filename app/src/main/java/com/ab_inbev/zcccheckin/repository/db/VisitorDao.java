package com.ab_inbev.zcccheckin.repository.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ab_inbev.zcccheckin.model.Visitor;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;

/**
 * Created by ETikhonov on 2018-01-19
 * Contains the methods used for accessing the database using Room DAOs
 */

@Dao
public interface VisitorDao {
    @Query("SELECT * FROM tblVisitors")
    Flowable<List<Visitor>> getAllVisitors();

    @Query("SELECT * FROM tblVisitors WHERE checkoutDateTime IS NULL")
    Maybe<List<Visitor>> getVisitorsInOffice();

    @Query("SELECT * FROM tblVisitors WHERE checkoutDateTime IS NOT NULL AND isSynced = 0")
    Maybe<List<Visitor>> getVisitorsToSync();

    @Query("DELETE FROM tblVisitors WHERE checkInDate < :date")
    int deleteOlddata(long date);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertVisitor(Visitor visitor);

    @Update
    int updateVisitor(Visitor visitor);


}

package com.ab_inbev.zcccheckin.repository.db;

/**
 * Created by ETikhonov on 2018-01-19.
 * Contains the database holder
 * <p>
 * 2018-02-04 add MIGRATION_1_2
 * 2018-04-15 add MIGRATION_2_3
 */

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;

import com.ab_inbev.zcccheckin.model.Visitor;

import org.joda.time.DateTimeZone;

import java.util.UUID;

@Database(entities = {Visitor.class}, version = 3)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE tblVisitors2 (" +
                    "uuid TEXT NOT NULL," +
                    "checkInDate INTEGER NOT NULL," +
                    "checkInTime INTEGER NOT NULL," +
                    "idCardNumber INTEGER NOT NULL," +
                    "visitorsName TEXT NOT NULL," +
                    "department TEXT," +
                    "manager TEXT," +
                    "checkoutDateTime INTEGER," +
                    "comments TEXT," +
                    "isSynced INTEGER NOT NULL," +
                    "checkpoint TEXT," +
                    "PRIMARY KEY(uuid));");

            Cursor cursor = database.query("SELECT * FROM tblVisitors");
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ContentValues values = new ContentValues();
                DateTimeZone tz = DateTimeZone.getDefault();
                values.put("uuid", UUID.randomUUID().toString());
                values.put("checkInDate", tz.convertLocalToUTC(cursor.getLong(0), true));
                values.put("checkInTime", tz.convertLocalToUTC(cursor.getLong(1), true));
                values.put("idCardNumber", cursor.getInt(2));
                values.put("visitorsName", cursor.getString(3));
                values.put("department", cursor.getString(4));
                values.put("manager", cursor.getString(5));
                values.put("checkoutDateTime", cursor.getLong(6));
                values.put("comments", cursor.getString(7));
                values.put("isSynced", cursor.getInt(8));
                values.put("checkpoint", cursor.getString(9));
                database.insert("tblVisitors2", 0, values);
                cursor.moveToNext();
            }
            cursor.close();
            database.execSQL("DROP TABLE tblVisitors;");
            database.execSQL("ALTER TABLE tblVisitors2 RENAME TO tblVisitors;");
        }
    };
    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE tblVisitors "
                    + " ADD COLUMN checkpoint TEXT");
        }
    };
    private static final String DATABASE_NAME = "visitors-database";
    private static final Object lock = new Object();
    private static AppDatabase sInstance;

    public static AppDatabase getAppDatabase(Context context) {
        return getInstance(context);
    }

    public static AppDatabase getInstance() {
        return sInstance;
    }

    public static void destroyInstance() {
        sInstance = null;
    }

    public static AppDatabase getInstance(final Context context) {
        if (sInstance == null) {
            synchronized (lock) {
                if (sInstance == null) {
                    sInstance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, DATABASE_NAME)
                            .addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                            .build();
                }
            }
        }
        return sInstance;
    }

    public abstract VisitorDao visitorDao();
}

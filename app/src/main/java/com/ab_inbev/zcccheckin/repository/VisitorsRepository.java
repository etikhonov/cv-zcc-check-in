package com.ab_inbev.zcccheckin.repository;

import android.content.Context;
import android.preference.PreferenceManager;

import com.ab_inbev.zcccheckin.R;
import com.ab_inbev.zcccheckin.model.GoogleSheetBatchPostData;
import com.ab_inbev.zcccheckin.model.Visitor;
import com.ab_inbev.zcccheckin.repository.api.SheetService;
import com.ab_inbev.zcccheckin.repository.db.AppDatabase;
import com.ab_inbev.zcccheckin.repository.db.VisitorDao;
import com.ab_inbev.zcccheckin.util.Constants;
import com.fatboyindustrial.gsonjodatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ETikhonov on 2018-01-15
 */

public class VisitorsRepository {

    private VisitorDao visitorDao;
    private Context context;

    public VisitorsRepository(Context context) {
        this.context = context;
        this.visitorDao = AppDatabase.getInstance(context).visitorDao();
    }

    public Observable<List<Visitor>> getVisitors() {
        return getVisitorsFromDb();
    }

    private Observable<List<Visitor>> getVisitorsFromDb() {
        return visitorDao.getAllVisitors()
                .toObservable();
    }

    public Observable<List<Visitor>> getVisitorsInOffice() {
        return visitorDao.getVisitorsInOffice()
                .toObservable();
    }

    public Observable storeVisitorInDb(Visitor visitor) {
        return Observable.fromCallable(() -> visitorDao.insertVisitor(visitor))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable updateVisitor(Visitor visitor) {
        return Observable.fromCallable(() -> visitorDao.updateVisitor(visitor))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable deleteOldVisitors(long date) {
        return Observable.fromCallable(() -> visitorDao.deleteOlddata(date))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable SyncWithGoogleSheet() {
        return visitorDao.getVisitorsToSync()
                .toObservable()
                .flatMap(visitors -> {
                    GoogleSheetBatchPostData postData = visitorListToGSheetBatchPostData(visitors);
                    return SheetService.getVisitorService()
                            .sendNewVisitorsRx(Constants.GOOGLE_SCRIPT_ID, postData);
                })
                .flatMap(visitors -> Observable.fromArray(visitors))
                .doOnNext(visitor -> visitor.setSynced(true))
                .flatMap(visitor -> updateVisitor(visitor));
    }

    private GoogleSheetBatchPostData visitorListToGSheetBatchPostData(List<Visitor> visitors) {
        String sheetId = PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(Constants.PREF_KEY_GOOGLE_SHEET_ID,
                        context.getString(R.string.pref_default_google_sheet_id));

        //Convert list of visitors to Array for converting it to json string
        //visitors.toArray() throws java.lang.Object[] cannot be cast to com.ab_inbev.zcccheckin.a.b[]
        int i = visitors.size();
        Visitor[] visitorsArray = new Visitor[i];
        for (int j = 0; j < i; j++) {
            visitorsArray[j] = visitors.get(j);
        }

        Gson gson = Converters.registerAll(new GsonBuilder()).create();

        GoogleSheetBatchPostData postData = new GoogleSheetBatchPostData();
        postData.sheetId = sheetId;
        postData.visitor = gson.toJson(visitorsArray);
        return postData;
    }
}

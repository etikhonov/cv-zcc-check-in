package com.ab_inbev.zcccheckin.repository.db;

import android.arch.persistence.room.TypeConverter;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

/**
 * Created by ETikhonov on 2018-01-21.
 * TypeConverters for Room
 */

public class Converters {

    private static DateTimeZone defaultTimeZone = DateTimeZone.getDefault();

    @TypeConverter
    public static LocalDate localDateFromTimestamp(Long value) {
        return value == null ? null : new LocalDate(defaultTimeZone.convertUTCToLocal(value));
    }

    @TypeConverter
    public static Long localDateToTimestamp(LocalDate date) {
        if (date != null) {
            long localMillis = date.toDateTimeAtStartOfDay(defaultTimeZone).getMillis();
            return defaultTimeZone.convertLocalToUTC(localMillis, true);
        } else {
            return null;
        }
    }

    @TypeConverter
    public static LocalTime localTimeFromTimestamp(Long value) {
        return value == null ? null :
                LocalTime.fromMillisOfDay(defaultTimeZone.convertUTCToLocal(value));
    }

    @TypeConverter
    public static Long localTimeToTimestamp(LocalTime time) {
        if (time != null) {
            return defaultTimeZone.convertLocalToUTC(time.getMillisOfDay(), false);
        } else {
            return null;
        }
    }

    @TypeConverter
    public static LocalDateTime localDateTimeFromTimestamp(Long value) {
        return value == null ? null : new LocalDateTime(value);
    }

    @TypeConverter
    public static Long localDateTimeToTimestamp(LocalDateTime localDateTime) {
        if (localDateTime != null) {
            DateTime dt = localDateTime.toDateTime(DateTimeZone.getDefault());
            return dt.getMillis();
        } else {
            return null;
        }
    }


}

package com.ab_inbev.zcccheckin.repository.api;

import com.ab_inbev.zcccheckin.model.GoogleSheetBatchPostData;
import com.ab_inbev.zcccheckin.model.Visitor;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;


public interface VisitorService {

    @POST("macros/s/{scriptId}/exec")
    Observable<Visitor[]> sendNewVisitorsRx(@Path("scriptId") String scriptId,
                                            @Body GoogleSheetBatchPostData postData);
}

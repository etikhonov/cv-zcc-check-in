package com.ab_inbev.zcccheckin.services;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.preference.PreferenceManager;

import com.ab_inbev.zcccheckin.repository.VisitorsRepository;
import com.ab_inbev.zcccheckin.util.Constants;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by ETikhonov on 2018-02-04.
 * Service for clean old data
 */

public class DataCleanerService extends JobService {

    final int DEFAULT_DATA_CLEANSING_PERIOD = 30;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    public boolean onStartJob(JobParameters params) {
        int dataCleansingPeriod;
        String prefDatacleansingPeriod = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext())
                .getString(Constants.PREF_KEY_DATA_CLEANSING, "nfe");
        try {
            dataCleansingPeriod = Integer.parseInt(prefDatacleansingPeriod);
        } catch (NumberFormatException nfe) {
            dataCleansingPeriod = DEFAULT_DATA_CLEANSING_PERIOD;
        }
        LocalDate localDate = LocalDate.now();
        long dateFromDelete = DateTime.parse(localDate.minusDays(dataCleansingPeriod).toString()).getMillis();
        compositeDisposable.add(
                new VisitorsRepository(this)
                        .deleteOldVisitors(dateFromDelete)
                        .subscribe(it -> jobFinished(params, true)));
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
        return true;
    }
}

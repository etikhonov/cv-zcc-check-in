package com.ab_inbev.zcccheckin.services;

import android.app.job.JobParameters;
import android.app.job.JobService;

import com.ab_inbev.zcccheckin.repository.VisitorsRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SyncJobService extends JobService {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    public boolean onStartJob(JobParameters params) {
        compositeDisposable.add(
                new VisitorsRepository(SyncJobService.this)
                        .SyncWithGoogleSheet()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(it -> jobFinished(params, true)));
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
        return true;
    }
}

package com.ab_inbev.zcccheckin.receivers;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;

import com.ab_inbev.zcccheckin.services.DataCleanerService;
import com.ab_inbev.zcccheckin.services.SyncJobService;
import com.ab_inbev.zcccheckin.util.Constants;

import java.util.concurrent.TimeUnit;

/**
 * Created by ETikhonov on 2018-01-23.
 * BroadcastReceiver for scheduling jobs on BOOT_COMPLETED
 */

public class BootReceiver extends BroadcastReceiver {

    final int DEFAULT_SYNC_FREQUENCY = 30;

    @Override
    public void onReceive(Context context, Intent intent) {
        scheduleSync(context);
        scheduleDataClean(context);
    }

    private void scheduleSync(Context context) {
        int syncFrequency;
        String prefSyncFrequency = PreferenceManager
                .getDefaultSharedPreferences(context)
                .getString(Constants.PREF_KEY_SYNC_FREQUENCY, "nfe");
        try {
            syncFrequency = Integer.parseInt(prefSyncFrequency);
        } catch (NumberFormatException nfe) {
            syncFrequency = DEFAULT_SYNC_FREQUENCY;
        }
        ComponentName serviceComponent = new ComponentName(context, SyncJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .setPeriodic(TimeUnit.MINUTES.toMillis(syncFrequency));
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
    }

    private void scheduleDataClean(Context context) {
        ComponentName serviceComponent = new ComponentName(context, DataCleanerService.class);
        JobInfo.Builder builder = new JobInfo.Builder(1, serviceComponent);
        builder.setPersisted(true)
                .setPeriodic(TimeUnit.DAYS.toMillis(1));
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
    }
}

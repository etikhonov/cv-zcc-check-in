ZCC Checkin
===============

ZCC Checkin is an android app for registering visitors in electronic form (Google Sheets) to replace guestbooks or sign in sheets in AB Inbev Business Service Center Kharkiv.

Download Demo App
-------------------------

<a href="README_files/com.ab_inbev.zcccheckin.apk">
 <img alt="launcher icon" src="README_files/launcher.PNG"  width="50" height="50"/>
</a>

Requirements
-------------
- API Level 23 (ICS) and above.  
- xlargeScreen

Used Libs
-------------
- [Butter Knife][3]
- [joda-time][4]
- [RxJava][5]
- [CircularDialogs][6]
- [Retrofit][7]
- [google-gson][9]
- [gson-jodatime-serialisers][8]
- [Room Persistence Library][10]
- [Firebase Crashlytics][11]

How to use
------------
- Start the app
- Do checkin and chekout actions
- Wait about 1 hour or Sync data immediate:
    * open Visitors List (eye button)
    * swipe from left 2 right
    * touch Sync Now Button
- Check data on [Google sheet][1]

Available settings
---------------------
- Customisable Background and Logo images
- Customisable Welcome Text 
- Customisable Sync and Data cleansing periods

App Screenshots
------------
<img alt="app Screenshot" src="README_files/Screenshot_1523990303.png"  width="256" height="160"/>
<img alt="app Screenshot" src="README_files/Screenshot_1523990413.png"  width="256" height="160"/>
<img alt="app Screenshot" src="README_files/Screenshot_1523990701.png"  width="256" height="160"/>
<img alt="app Screenshot" src="README_files/Screenshot_1523990725.png"  width="256" height="160"/>
<img alt="app Screenshot" src="README_files/Screenshot_1523990741.png"  width="256" height="160"/>
<img alt="app Screenshot" src="README_files/Screenshot_1523990754.png"  width="256" height="160"/>

How to build
-------------
- [Deploy script from /README_files/script as a web app][2]
- Set script Id in util/Constants.GOOGLE_SCRIPT_ID
- Set sheet Id in string resource with name=pref_default_google_sheet_id

[1]: https://docs.google.com/spreadsheets/d/1l5YTtLxv28PAwka0V_9rAkj8J_rFeSgLNE7Wze97CIg/
[2]: https://developers.google.com/apps-script/guides/web
[3]: http://jakewharton.github.io/butterknife/
[4]: http://www.joda.org/joda-time/
[5]: https://github.com/ReactiveX/RxJava
[6]: https://github.com/HassanUsman/CircularDialogs
[7]: http://square.github.io/retrofit/
[8]: https://github.com/gkopff/gson-jodatime-serialisers
[9]: https://github.com/google/gson
[10]: https://developer.android.com/topic/libraries/architecture/room.html
[11]: https://firebase.google.com/docs/crashlytics/
